FROM python:3.6-buster

COPY . /assignments

WORKDIR /assignments

RUN chmod +x */*.py
RUN make all

WORKDIR /assignments
ENTRYPOINT ["./run.sh"]
