
sources = $(dir $(wildcard */Makefile))

all:
	for path in $(sources) ; do $(MAKE) -C $$path all; done

clean:
	for path in $(sources) ; do $(MAKE) -C $$path clean; done