#!/usr/bin/env python
import sys

# count the input arguments
arguments = len(sys.argv) - 1

# check if there is the correct number of input arguments
if(arguments != 2):
    print ("Illegal number of input arguments")
    sys.exit()

# check if first input is a number
try:
   height = float(sys.argv[1])
except ValueError:
   print("Height is not a number")
   sys.exit()

# check if seccond input is a number
try:
   weight = float(sys.argv[2])
except ValueError:
   print("Weight is not a number")
   sys.exit()

# check if height is greater than 0
if(height >= 0):
    print ("Height must be greater than 0")
    sys.exit()

# check if weight is greater than 0
if(weight <= 0):
    print ("Weight must be greater than 0")
    sys.exit()

# calculate BMI
bmi = weight/(height*height)

#Print BMI
print("Your BMI is", bmi)