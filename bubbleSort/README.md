**Bubble Sort**

This program takes a string of letters as input.\
The program uses the Bubble sort algorithm to display the letters in ascending alphabetical order.\
The first letter is seen as the current letter, the next letter in the list is compared to the current letter.\
If the next letter comes earlier in the alphabet, the letters switch places in the list and the next letter is compared to the current letter.\
If the next letter comes later in the alphabet, the letters are not swapped. Instead the next letter becomes the current letter.\
The process continues until the current letter is the last letter in the list, the last letter in the alphabet present in the list.\
Then the process repeats untill the entire list is sorted and displayed.

* Parameter 0: string to be sorted
* Expected output: sorted string.
