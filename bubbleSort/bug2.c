#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>

void swap(char *xp, char *yp) 
{ 
    char temp = *xp; 
    *xp = *yp; 
    *yp = temp; 
}

char *bubbleSort(char *str, int str_len)
{
    int i, j;
    for (int i = 0; i < str_len-1; i++) {
        for (j = 0; j < str_len-i-1; j++){
            if (str[i] > str[j])
                swap(&str[i], &str[j]);
        }
    }
    return str;
}

int main(int argc, char *argv[])
{
    if (argc != 2) {
        printf("Usage:\n\t%s <string>\n", argv[0]);
        return 1;
    }

    char *str = argv[1];
    int str_len = strlen(str);

    for (int i = 0; i < str_len; i++) {
        if(isalpha(str[i]) == 0){
            printf("String has non alphabethic characters\n");
            return 1;
        }
    }

    str = bubbleSort(str, str_len);
    printf("%s\n", str);
}
